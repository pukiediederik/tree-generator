﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CubeMeshData
{

    public enum Direction
    {
        up,
        down,
        forward,
        backward,
        left,
        right
    }

    public static Vector3[] vertices = {
        new Vector3( 0.5f ,  0.5f,  0.5f), //0
        new Vector3( 0.5f ,  0.5f, -0.5f), //1
        new Vector3( 0.5f , -0.5f,  0.5f), //2
        new Vector3( 0.5f , -0.5f, -0.5f), //3
        new Vector3(-0.5f,   0.5f,  0.5f), //4
        new Vector3(-0.5f,   0.5f, -0.5f), //5
        new Vector3(-0.5f,  -0.5f,  0.5f), //6
        new Vector3(-0.5f,  -0.5f, -0.5f)  //7
    };

    //based of vertices
    private static int[][] tris = {
    new int[] {0,5,4, 0,1,5},
    new int[] {2,6,7, 2,7,3},
    new int[] {0,4,2, 4,2,6},
    new int[] {7,5,3, 3,5,1},
    new int[] {6,5,7, 5,6,4},
    new int[] {1,0,2, 1,2,3},
    };

    private static int[][] faceVertices = {
    new int[] {0,1,4,5},
    new int[] {2,3,6,7},
    new int[] {0,2,4,6},
    new int[] {1,3,5,7},
    new int[] {4,5,6,7},
    new int[] {0,1,2,3}
    };

    //based of faceVertices
    private static int[][] faceTris = {
    new int[] {0,3,2, 0,1,3},
    new int[] {0,2,3, 0,3,1},
    new int[] {0,2,1, 2,3,1},
    new int[] {3,2,1, 1,2,0},
    new int[] {0,1,2, 1,3,2},
    new int[] {1,0,2, 1,2,3},
    };

    //returns all vertices needed to create a face
    public static Vector3[] GetFaceVertices(Direction dir)
    {
        Vector3[] returnValue = new Vector3[4];

        for (int i = 0; i < 4; i++)
        {
            returnValue[i] = vertices[faceVertices[(int)dir][i]];
        }

        return returnValue;
    }

    //returns the
    public static int[] GetFaceTris(Direction dir)
    {
        //TODO
        return faceTris[(int)dir];
    }
}
