﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeGenerator : MonoBehaviour
{
    //wind variables
    public Vector2 windDirection; //the direction the wind comes from
    public float windForce; //how much effect the wind has on the tree

    //trunk variables
    [Header("Trunk")]
    public float trunkHeight;
    public float trunkNoiseAmount; //how much the noise will effect the shape of the tree
    public int trunkVerticeAmount; //the amount of vertices in the trunk
    public float groundRadius;
    public float crownRadius;

    [Space]

    //branch variables
    [Header("Branch")]
    public int branchAmount; //the amount of branches
    [Range(0,180)] public float branchMinAngle; //degrees
    [Range(0,180)] public float branchMaxAngle; //degrees
    public int branchVerticeAmount;
    public float branchLength;
    public float branchNoiseAmount; //noise multiplier
    public float branchEndRadius; //the radius at the end of a branch

    public Vector3[] branchVectors;


    public Vector3[] trunkPositions;
    public Vector3[,] branchPositions;

    bool[,,] woodMap; //this is where all the logs will be 0:air, 1:logs
    Vector2Int woodMapOffset;

    public MeshFilter meshf;
    public MeshRenderer meshr;


    [EasyButtons.Button("Generate Tree")]
    public void GenerateTree(){
        //clear the developer console

        ClearConsole();

        //TODO: with my current settings this works, make sure to fix this later on
        voxelizationSpheres = new Stack<KeyValuePair<Vector3, float>>();
        falsepositions = new List<Vector3>();
        visulalizeSpheres = new Stack<KeyValuePair<Vector3, float>>(); //Only used for debugging

        windDirection.Normalize();
        trunkPositions = new Vector3[trunkVerticeAmount];

        //destroy all cubes on generating a new tree
        int childAmount = this.transform.childCount;
        for(int i = 0; i < childAmount; i++){
            DestroyImmediate(this.transform.GetChild(0).gameObject);
        }
        

        //start generating the trunk
        trunkPositions[0] = Vector3.zero;
        for(int i = 1; i < trunkVerticeAmount; i++){
            trunkPositions[i] = new Vector3((windForce * windDirection.x) + (Random.Range(-1f,1f) * trunkNoiseAmount),
                                            (trunkHeight / (trunkVerticeAmount -1)),
                                            (windForce * windDirection.y) + (Random.Range(-1f,1f) * trunkNoiseAmount)) + trunkPositions[i-1];
        }

        //generating the branches
        branchVectors = new Vector3[branchAmount];

        //generating the vectors for the branches
        for(int i = 0; i < branchAmount; i++){
            float VerticalAngle = Random.Range(branchMinAngle,branchMaxAngle) - 90;
            float multiplier = Mathf.Cos(VerticalAngle * Mathf.Deg2Rad);
            branchVectors[i] = new Vector3(Mathf.Cos(Mathf.PI * 2 / branchAmount * i) * multiplier, //add wind to create a slight offset
                                           Mathf.Sin(VerticalAngle * Mathf.Deg2Rad),
                                           Mathf.Sin(Mathf.PI * 2 / branchAmount * i) * multiplier); //add wind to create a slight offset
            
        }

        branchPositions = new Vector3[branchAmount,branchVerticeAmount];

        //generating the branches
        for(int i = 0; i < branchAmount; i++){
            branchPositions[i,0] = trunkPositions[trunkPositions.Length - 1];
            for(int j = 1; j < branchVerticeAmount; j++){
                branchPositions[i,j] = branchVectors[i] * (branchLength / (branchVerticeAmount - 1)) * j + new Vector3(Random.Range(-1f,1f), Random.Range(-1f,1f), Random.Range(-1f,1f)) * branchNoiseAmount + trunkPositions[trunkPositions.Length - 1];
            }
        }

        //generate voxels
        woodMap = new bool[(Mathf.CeilToInt(branchLength) + 1) * 2, Mathf.CeilToInt(branchLength) + Mathf.CeilToInt(trunkHeight), (Mathf.CeilToInt(branchLength) + 1) * 2];
        Debug.Log($"Woodmap dimensions: ({woodMap.GetLength(0)}, {woodMap.GetLength(1)}, {woodMap.GetLength(2)})");
        woodMapOffset = new Vector2Int(Mathf.RoundToInt(-trunkPositions[trunkPositions.Length - 1].x + (woodMap.GetLength(0) /2)),
                                       Mathf.RoundToInt(-trunkPositions[trunkPositions.Length - 1].z + (woodMap.GetLength(2) /2)));

        //trunk voxels
        for(int i = 1; i < trunkVerticeAmount; i++){
            VoxelizeWood(trunkPositions[i-1], trunkPositions[i], groundRadius - (groundRadius - crownRadius) * ((float)(i - 1) / ((float)trunkVerticeAmount - 1)), 
                                                                 groundRadius - (groundRadius - crownRadius) * ((float)i / ((float)trunkVerticeAmount - 1)));
        }
        //branch voxels
        for(int i = 0; i < branchAmount; i++){
            for(int j = 1; j < branchVerticeAmount; j++){
                VoxelizeWood(branchPositions[i,j-1],branchPositions[i,j], crownRadius - (crownRadius - branchEndRadius) * (float)(j-1) / (float)(branchVerticeAmount -1),
                                                                          crownRadius - (crownRadius - branchEndRadius) * (float)j / (float)(branchVerticeAmount -1));
            }
        }

        GenerateMesh();
    }

    
    Stack<KeyValuePair<Vector3, float>> voxelizationSpheres = new Stack<KeyValuePair<Vector3, float>>();
    Stack<KeyValuePair<Vector3, float>> visulalizeSpheres = new Stack<KeyValuePair<Vector3, float>>();
    List<Vector3> falsepositions = new List<Vector3>();

    //voxelizes the trunk and branches
    public void VoxelizeWood(Vector3 startPosition, Vector3 endPosition, float startRadius, float endRadius){
        //position, radius
        //Stack<KeyValuePair<Vector3, float>> voxelizationSpheres = new Stack<KeyValuePair<Vector3, float>>();

        int itterations = Mathf.CeilToInt(Vector3.Distance(startPosition,endPosition));
        Vector3 direction = (endPosition - startPosition).normalized;
        float radiusDifference = startRadius - endRadius;

        for (int i = 0; i < itterations; i++){
            voxelizationSpheres.Push(new KeyValuePair<Vector3, float>(startPosition + direction * i, startRadius - radiusDifference / (float)itterations * i));
        }

        while(voxelizationSpheres.Count != 0){
            KeyValuePair<Vector3,float> sphere = voxelizationSpheres.Pop();
            visulalizeSpheres.Push(sphere);
            int radius = Mathf.CeilToInt(sphere.Value);

            for(int x = -radius; x <= radius; x++){
                for(int y = -radius; y <= radius; y++){
                    for(int z = -radius; z <= radius; z++){
                        //check distance
                        if(Vector3.Distance(Vector3.zero, Vector3Int.RoundToInt(sphere.Key) - sphere.Key + new Vector3(x,y,z)) <= sphere.Value){
                            //check if the position is in the bounding box
                            if( x + woodMapOffset.x + Vector3Int.RoundToInt(sphere.Key).x >= 0  && x + woodMapOffset.x + Vector3Int.RoundToInt(sphere.Key).x < woodMap.GetLength(0) &&
                                y + Mathf.RoundToInt(sphere.Key.y) >= 0 && y + Mathf.RoundToInt(sphere.Key.y) < woodMap.GetLength(1) &&
                                z + woodMapOffset.y + Vector3Int.RoundToInt(sphere.Key).z >= 0 && z + woodMapOffset.y + Vector3Int.RoundToInt(sphere.Key).z < woodMap.GetLength(2)){

                                //set the woodmap to true
                                woodMap[x + woodMapOffset.x + Vector3Int.RoundToInt(sphere.Key).x,
                                        y + Vector3Int.RoundToInt(sphere.Key).y,
                                        z + woodMapOffset.y + Vector3Int.RoundToInt(sphere.Key).z] = true;
                            }
                        }
                    }
                }
            }
        }
    }

    //visualizing the tree in the editor
    void OnDrawGizmos(){
        //drawing the trunk
        for(int i = 0; i < trunkPositions.Length - 1; i++){
            Gizmos.color = Color.green;
            Gizmos.DrawLine(trunkPositions[i], trunkPositions[i + 1]);
            Gizmos.color = new Color(0,.4f,0);
            Gizmos.DrawWireSphere(trunkPositions[i], groundRadius - ((groundRadius - crownRadius) * (trunkPositions[i].y / trunkHeight)));
        }
        Gizmos.DrawWireSphere(trunkPositions[trunkPositions.Length - 1], groundRadius - ((groundRadius - crownRadius) * (trunkPositions[trunkPositions.Length - 1].y / trunkHeight)));
        
        //draw the correct cube
        Gizmos.color = Color.white;
        Gizmos.DrawWireCube(new Vector3(trunkPositions[trunkPositions.Length - 1].x, woodMap.GetLength(1) / 2, trunkPositions[trunkPositions.Length - 1].z),
                            new Vector3(woodMap.GetLength(0), woodMap.GetLength(1), woodMap.GetLength(2)));


        //draw the max branch size
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(trunkPositions[trunkPositions.Length - 1], branchLength);

        //drawing branches
        Gizmos.color = Color.cyan;
        for(int i = 0; i < branchAmount; i++){
            for(int j = 1; j < branchVerticeAmount; j++){
                Gizmos.DrawLine(branchPositions[i,j - 1], branchPositions[i,j]);
            }
        }

        Gizmos.color = Color.magenta;
        foreach(KeyValuePair<Vector3,float> sphere in visulalizeSpheres){
            Gizmos.DrawWireSphere(sphere.Key, sphere.Value);
        }
        Gizmos.color = Color.red;

        foreach(Vector3 falsepos in falsepositions){
            Gizmos.DrawCube(falsepos, Vector3.one * .1f);
        }
    }

    [EasyButtons.Button]
    public void GenerateMesh(){
        Mesh mesh = TreeRenderer.GenerateMesh(woodMap, woodMapOffset);
        meshf.mesh.Clear();

        mesh.RecalculateNormals();
        Debug.Log("Generated tree with " + mesh.triangles.Length / 3 + " triangles");

        meshf.mesh = mesh;
    }

    static void ClearConsole(){
        var logEntries = System.Type.GetType("UnityEditor.LogEntries, UnityEditor.dll");

        var clearMethod = logEntries.GetMethod("Clear", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);

        clearMethod.Invoke(null, null);
    }

}
