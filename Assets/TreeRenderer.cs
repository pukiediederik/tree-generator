﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeRenderer{
    public static Mesh GenerateMesh(bool[,,] woodMap, Vector2 offset){

        Mesh mesh = new Mesh();
        List<Vector3> vertices = new List<Vector3>();
        List<int> tris = new List<int>();

        Vector3Int woodMapSize = new Vector3Int(woodMap.GetLength(0), woodMap.GetLength(1), woodMap.GetLength(2));

        //loop over each axis
        for (int x = 0; x < woodMapSize.x; x++){
            for (int y = 0; y < woodMapSize.y; y++){
                for (int z = 0; z < woodMapSize.z; z++){

                    //if there is wood at the current position
                    if (woodMap[x, y, z]) {
                        //showing left faces
                        if ((x > 0 && !woodMap[x - 1, y, z]) || x == 0) {
                            //Add Face
                            Vector3[] faceVertices = CubeMeshData.GetFaceVertices(CubeMeshData.Direction.left);
                            foreach (Vector3 vertice in faceVertices) {
                                vertices.Add(vertice + new Vector3(x, y, z) - new Vector3(offset.x, 0, offset.y));
                            }

                            int[] faceTris = CubeMeshData.GetFaceTris(CubeMeshData.Direction.left);
                            int l = tris.Count / 6;

                            foreach (int i in faceTris) {
                                tris.Add(i + (l * 4));
                            }
                        }
                        //showing right faces
                        if ((x < woodMapSize.x - 1 && !woodMap[x + 1, y, z]) || x == woodMapSize.x - 1){
                            //Add Face
                            Vector3[] faceVertices = CubeMeshData.GetFaceVertices(CubeMeshData.Direction.right);
                            foreach (Vector3 vertice in faceVertices){
                                vertices.Add(vertice + new Vector3(x, y, z) - new Vector3(offset.x, 0, offset.y));
                            }

                            int[] faceTris = CubeMeshData.GetFaceTris(CubeMeshData.Direction.right);
                            int l = tris.Count / 6;

                            foreach (int i in faceTris){
                                tris.Add(i + (l * 4));
                            }
                        }

                        //showing downward faces
                        if ((y > 0 && !woodMap[x, y - 1, z]) || y == 0)
                        {
                            //Add Face
                            Vector3[] faceVertices = CubeMeshData.GetFaceVertices(CubeMeshData.Direction.down);
                            foreach (Vector3 vertice in faceVertices)
                            {
                                vertices.Add(vertice + new Vector3(x, y, z) - new Vector3(offset.x, 0, offset.y));
                            }

                            int[] faceTris = CubeMeshData.GetFaceTris(CubeMeshData.Direction.down);
                            int l = tris.Count / 6;

                            foreach (int i in faceTris)
                            {
                                tris.Add(i + (l * 4));
                            }
                        }
                        //showing up faces
                        if ((y < woodMapSize.y - 1 && !woodMap[x, y + 1, z]) || y == woodMapSize.y - 1)
                        {
                            //Add Face
                            Vector3[] faceVertices = CubeMeshData.GetFaceVertices(CubeMeshData.Direction.up);
                            foreach (Vector3 vertice in faceVertices)
                            {
                                vertices.Add(vertice + new Vector3(x, y, z) - new Vector3(offset.x, 0, offset.y));
                            }

                            int[] faceTris = CubeMeshData.GetFaceTris(CubeMeshData.Direction.up);
                            int l = tris.Count / 6;

                            foreach (int i in faceTris)
                            {
                                tris.Add(i + (l * 4));
                            }
                        }

                        //showing backwards faces
                        if ((z > 0 && !woodMap[x, y, z - 1]) || z == 0)
                        {
                            //Add Face
                            Vector3[] faceVertices = CubeMeshData.GetFaceVertices(CubeMeshData.Direction.backward);
                            foreach (Vector3 vertice in faceVertices)
                            {
                                vertices.Add(vertice + new Vector3(x, y, z) - new Vector3(offset.x, 0, offset.y));
                            }

                            int[] faceTris = CubeMeshData.GetFaceTris(CubeMeshData.Direction.left);
                            int l = tris.Count / 6;

                            foreach (int i in faceTris)
                            {
                                tris.Add(i + (l * 4));
                            }
                        }              
                        //showing forward faces
                        if ((z < woodMapSize.z - 1 && !woodMap[x, y, z + 1]) || z == woodMapSize.z - 1)
                        {
                            //Add Face
                            Vector3[] faceVertices = CubeMeshData.GetFaceVertices(CubeMeshData.Direction.forward);
                            foreach (Vector3 vertice in faceVertices)
                            {
                                vertices.Add(vertice + new Vector3(x, y, z) - new Vector3(offset.x, 0, offset.y));
                            }

                            int[] faceTris = CubeMeshData.GetFaceTris(CubeMeshData.Direction.right);
                            int l = tris.Count / 6;

                            foreach (int i in faceTris)
                            {
                                tris.Add(i + (l * 4));
                            }
                        }
                    }
                }
            }
        }

        mesh.vertices = vertices.ToArray();
        mesh.triangles = tris.ToArray();

        return mesh;
    }
}
