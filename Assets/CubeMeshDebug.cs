﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMeshDebug : MonoBehaviour
{
    public Color defaultColor = Color.blue;
    public Color highlightColor = Color.red;

    public MeshFilter meshFilter;
    public MeshRenderer meshRenderer;

    public bool showFaceDebug;

    public int showface = 0;
    private void OnDrawGizmos()
    {
        Gizmos.color = defaultColor;

        if (showFaceDebug)
        {
            foreach (Vector3 pos in CubeMeshData.GetFaceVertices((CubeMeshData.Direction)showface))
            {
                Gizmos.DrawSphere(pos, .2f);
            }
        }
    }

    public Mesh GenerateCubeMesh() {
        Mesh mesh = new Mesh();

        Vector3[] vertices = new Vector3[4 * 6];
        int[] tris = new int[6 * 6];

        for (int i = 0; i < 6; i++)
        {
            Vector3[] faceVertices = CubeMeshData.GetFaceVertices((CubeMeshData.Direction)i);
            
            for (int j = 0; j < 4; j++) {
                vertices[(i * 4) + j] = faceVertices[j];
            }

            int[] faceTris = CubeMeshData.GetFaceTris((CubeMeshData.Direction)i);

            for (int j = 0; j < 6; j++)
            {
                tris[(i * 6) + j] = faceTris[j] + (i * 4);
            }
        }

        mesh.vertices = vertices;
        mesh.triangles = tris;

        return mesh;
    }

    [EasyButtons.Button]
    public void GenerateCube() {
        Mesh mesh = GenerateCubeMesh();

        meshFilter.mesh.Clear();

        meshFilter.mesh = mesh;
    }
}
